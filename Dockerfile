FROM node:12.13.1 AS web
WORKDIR /web
COPY . .
RUN npm install && ng build --prod
