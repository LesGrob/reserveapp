import { Component, Input, Output, EventEmitter } from '@angular/core';

@Component({
    selector: 'weekday-component',
    templateUrl: './weekDay.component.html',
    styleUrls: ['./weekDay.component.css']
})
export class WeekDayComponent {
    weekDay: String = null;
    dayS: String = null;
    dayE: String = null;
    isActive: boolean = false;

    onClick(evt) {
        if (evt.target.id == "dayBody")
            return;
        if (evt.target.closest('#dayBody'))
            return;
        this.active = !this.active;
    }

    @Input()
    set day(weekDay: String) {
        this.weekDay = weekDay;
    }

    @Input()
    get start() {
        return this.dayS;
    }

    @Output() startChange = new EventEmitter();
    set start(val: String) {
        this.dayS = val;
        this.startChange.emit(this.dayS);
    }


    @Input() get end() {
        return this.dayE;
    }
    @Output() endChange = new EventEmitter();
    set end(val: String) {
        this.dayE = val;
        this.endChange.emit(this.dayE);
    }

    @Input() get active() {
        return this.isActive;
    }
    @Output() activeChange = new EventEmitter();
    set active(val: boolean) {
        this.isActive = val;
        this.activeChange.emit(this.isActive);
    }
}