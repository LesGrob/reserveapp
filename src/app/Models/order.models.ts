import { Branch } from './branch.models';
import { Table } from './table.models';
import { OrderStatus, TimeTransform } from './enums.models';
import { formatDate } from "@angular/common";
import { CalendarEvent } from 'calendar-utils';

export class Order {
    id: number = 0;
    status: OrderStatus = 1;
    name: string = "";
    surname: string = "";
    email: string = "";
    phone: string = "";
    time: Date = new Date();
    timeMinutes: number = 0;
    tables: Array<number> = [];
    peopleCount: number = 1;
    branch: Branch = new Branch();

    toJson() {
        return {
            id: this.id,
            status: this.status,
            name: this.name,
            surname: this.surname,
            email: this.email,
            phone: this.phone,
            time: this.time.toDateString(),
            timeMinutes: this.timeMinutes,
            tables: this.tables,
            peopleCount: this.peopleCount,
            branch: this.branch.toJson()
        }
    }

    static fromJson(data: any): Order {
        if (data == null) return null;
        let res = new Order();
        res.id = data.id;
        res.status = data.status;
        res.name = data.name;
        res.surname = data.surname;
        res.email = data.email;
        res.phone = data.phone;
        res.time = new Date(data.time);
        res.tables = data.tables.map(data => Table.fromJson(data));
        res.peopleCount = data.peopleCount;
        res.branch = Branch.fromJson(data.branch);
        return res;
    }
}

export class OrderFull {
    id: number = 0;
    status: OrderStatus = 1;
    name: string = "";
    surname: string = "";
    email: string = "";
    phone: string = "";
    time: Date = new Date();
    tables: Array<Table> = [];
    branch: Branch = new Branch();

    toJson() {
        return {
            id: this.id,
            status: this.status,
            name: this.name,
            surname: this.surname,
            email: this.email,
            phone: this.phone,
            time: this.time,
            tables: this.tables.map(data => data.toJson()),
            branch: this.branch.toJson()
        }
    }


    toCalendarEvent(): CalendarEvent {
        let title = formatDate(this.time, "HH:mm", 'ru') + ", Столы: ";
        for (let i = 0; i < this.tables.length; i++) {
            title += "#" + this.tables[i].id;
            if (i < (this.tables.length - 1)) title += ", "
        }
        let currentDate: Date = new Date();
        let end = new Date(this.time);
        end.setMinutes(this.time.getMinutes() + TimeTransform.toMinutes(this.branch.schedule.reservationLength));
        return {
            id: this.id,
            title: title,
            start: this.time,
            end: end,
            color: (this.time < currentDate ? colors.red : colors.green)
        }
    }

    static fromJson(data: any): OrderFull {
        if (data == null) return null;
        let res = new OrderFull();
        res.id = data.id;
        res.status = data.status;
        res.name = data.name;
        res.surname = data.surname;
        res.email = data.email;
        res.phone = data.phone;
        res.time = new Date(data.time);
        res.tables = data.tables.map(data => Table.fromJson(data));
        res.branch = Branch.fromJson(data.branch);
        return res;
    }
}

const colors: any = {
    red: {
        primary: '#ad2121',
        secondary: '#FAE3E3'
    },
    green: {
        primary: '#4caf50',
        secondary: '#4caf504d'
    }
};