export enum Status {
    NotPaid = 1,
    Paid = 2,
    Waiting = 3
}

export enum StatusDescription {
    "Не оплачено" = 1,
    "Оплачено" = 2,
    "В ожидании" = 3
}

export enum ReservationStatus {
    Finished = 1,
    Active = 2,
    Disabled = 3
}

export enum OrderStatus {
    Accepted = 1,
    Cancelled = 2
}

export enum WeekDay {
    "ВС" = 0,
    "ПН" = 1,
    "ВТ" = 2,
    "СР" = 3,
    "ЧТ" = 4,
    "ПТ" = 5,
    "СБ" = 6
}

export class TimeTransform {
    //time method
    static transformMinutes(mins: number): String {
        let hours = Math.floor(mins / 60); let minutes = mins % 60;
        let h = hours < 10 ? ("0" + hours) : hours; let m = minutes < 10 ? ("0" + minutes) : minutes;
        return h + ":" + m;
    }

    static transform(date: Date): String {
        let hours = date.getHours(); let minutes = date.getMinutes();
        let h = hours < 10 ? ("0" + hours) : hours; let m = minutes < 10 ? ("0" + minutes) : minutes;
        return h + ":" + m;
    }

    static toMinutes(time: String): number {
        let regex = "([0-9]?[0-9])\:([0-9]?[0-9])";
        let match = time.match(regex);
        if(match == null || match.length < 3) return 0;
        return (parseInt(match[1]) * 60 + parseInt(match[2]));
    }
}