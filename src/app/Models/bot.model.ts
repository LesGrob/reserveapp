export class CompanyBot {
    id: number = 0;
    branchId: number = 0;
    telegramGuid: string = "";
    telegramChatId: number = 0;

    toJson() {
        return {
            id: this.id,
            branchId: this.branchId,
            telegramGuid: this.telegramGuid,
            telegramChatId: this.telegramChatId
        }
    }

    static fromJson(data: any): CompanyBot {
        if (data == null) return null;
        let res = new CompanyBot();
        res.id = data.id;
        res.branchId = data.branchId;
        res.telegramGuid = data.telegramGuid;
        res.telegramChatId = data.telegramChatId;
        return res;
    }
}