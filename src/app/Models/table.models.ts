export class Table {
    constructor(description: string, positionX: number, positionY: number) {
        this.description = description;
        this.positionX = positionX;
        this.positionY = positionY;
    }
    id: number = 0;
    description: string = "";
    sits: number = 1;
    depositCost: number = 0;
    width: number = 40;
    height: number = 40;
    radius: number = 4;
    positionX: number = 0;
    positionY: number = 0;

    //Client data
    selected: boolean = false;
    availiable: boolean = false;

    deleted: boolean = false;

    toJson() {
        return {
            id: this.id,
            description: this.description,
            sits: this.sits,
            depositCost: this.depositCost,
            width: Math.round(this.width),
            height: Math.round(this.height),
            radius: this.radius,
            positionX: Math.round(this.positionX),
            positionY: Math.round(this.positionY)
        }
    }

    clone(): Table {
        let table = new Table(this.description, this.positionX, this.positionY);

        table.id = this.id;
        //table.description = this.description;
        table.sits = this.sits;
        table.depositCost = this.depositCost;
        table.width = this.width;
        table.height = this.height;
        table.radius = this.radius;
        //table.positionX = this.positionX;
        //table.positionY = this.positionY;

        return table;
    }

    static fromJson(data: any): Table {
        if (data == null) return null;
        let res = new Table(data.description, data.positionX, data.positionY);
        res.id = data.id;
        res.sits = data.sits;
        res.depositCost = data.depositCost;
        res.width = data.width;
        res.height = data.height;
        res.radius = data.radius;
        res.availiable = data.availiable;
        return res;
    }
}