import { Branch } from './branch.models';
import { TimeTransform } from './enums.models';

export class DisabledTime {
    id: number = 0;
    branch: Branch = new Branch();
    dateStart: Date = new Date();
    timeStart: String = "00:00";
    dateEnd: Date = new Date();
    timeEnd: String = "00:00";

    toJson() {
        let regex = "([0-9]?[0-9])\:([0-9]?[0-9])";
        let startMinutes = parseInt(this.timeStart.match(regex)[1]) * 60 + parseInt(this.timeStart.match(regex)[2]);
        let endMinutes = parseInt(this.timeEnd.match(regex)[1]) * 60 + parseInt(this.timeEnd.match(regex)[2])

        return {
            id: this.id,
            branch: this.branch.toJson(),
            timeStart: this.dateStart.toDateString(),
            timeEnd: this.dateEnd.toDateString(),
            startMinutes: startMinutes,
            endMinutes: endMinutes
        }
    }

    static fromJson(data: any): DisabledTime {
        if (data == null) return null;
        let res = new DisabledTime();
        res.id = data.id;
        res.branch = Branch.fromJson(data.branch);
        res.dateStart = new Date(data.timeStart);
        res.timeStart = TimeTransform.transformMinutes(data.startMinutes);
        res.dateEnd = new Date(data.timeEnd);
        res.timeEnd = TimeTransform.transformMinutes(data.endMinutes);
        return res;
    }
}