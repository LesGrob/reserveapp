import { Table } from "./table.models";

export class Zone {
    id: number = 0;
    name: string = "Зона"
    // guid: string = "00000000-0000-0000-0000-000000000000";
    imagePath: string = "";
    imageWidth: number = 0;
    imageHeight: number = 0;
    color: string = "rgba(255,255,255,1)";
    tables: Array<Table> = [];
    zoneImage: any;
    zoneImageName: string;
    temporary: boolean;

    tableColor: string = "rgba(0, 0, 0, 0.5)";
    tableBColor: string = "rgba(208, 208, 208, 1)";

    tableActiveColor: string = "rgba(0, 195, 0, 0.5)";
    tableActiveBColor: string = "rgba(208, 208, 208, 1)";

    tableDisabledColor: string = "rgba(199, 199, 199, 0.6)";
    tableDisabledBColor: string = "rgba(208, 208, 208, 1)";

    deleted: boolean = false;

    toJson() {
        return {
            id: this.id,
            name: this.name,
            // guid: this.guid,
            imagePath: this.imagePath.length > 100 ? "" : this.imagePath.replace("/Resources/ZoneImages/", ""),
            imageWidth: this.imageWidth,
            imageHeight: this.imageHeight,
            color: this.color,
            tables: this.tables.map(data => data.toJson()),
            zoneImage: this.zoneImage,
            zoneImageName: this.zoneImageName,
            temporary: this.temporary,

            tableColor: this.tableColor,
            tableBColor: this.tableBColor,
            tableActiveColor: this.tableActiveColor,
            tableActiveBColor: this.tableActiveBColor,
            tableDisabledColor: this.tableDisabledColor,
            tableDisabledBColor: this.tableDisabledBColor,
        }
    }

    clone(): Zone {
        let zone = new Zone();

        zone.id = this.id;
        zone.name = this.name;
        zone.imagePath = this.imagePath;
        zone.imageWidth = this.imageWidth;
        zone.imageHeight = this.imageHeight;
        zone.color = this.color;
        zone.tables = Object.assign([], this.tables.map(x => x.clone()));
        zone.zoneImage = this.zoneImage;
        zone.zoneImageName = this.zoneImageName;
        zone.temporary = this.temporary;
        zone.tableColor = this.tableColor;
        zone.tableBColor = this.tableBColor;
        zone.tableActiveColor = this.tableActiveColor;
        zone.tableActiveBColor = this.tableActiveBColor;
        zone.tableDisabledColor = this.tableDisabledColor;
        zone.tableDisabledBColor = this.tableDisabledBColor;

        return zone;
    }

    static fromJson(data: any): Zone {
        if (data == null) return null;
        let res = new Zone();
        res.id = data.id;
        res.name = data.name ? data.name : ("Зона " + data.id);
        res.imageWidth = data.imageWidth;
        res.imageHeight = data.imageHeight;
        res.imagePath = data.imagePath ? "/Resources/ZoneImages/" + data.imagePath : data.imagePath;
        res.color = data.color ? data.color : "#ffffff";
        res.tables = data.tables.map(data => Table.fromJson(data));
        res.temporary = data.temporary;

        res.tableColor = data.tableColor ? data.tableColor : "rgba(0, 0, 0, 0.5)";
        res.tableBColor = data.tableBColor ? data.tableBColor : "rgba(208, 208, 208, 1)";
        res.tableActiveColor = data.tableActiveColor ? data.tableActiveColor : "rgba(0, 195, 0, 0.5)";
        res.tableActiveBColor = data.tableActiveBColor ? data.tableActiveBColor : "rgba(208, 208, 208, 1)";
        res.tableDisabledColor = data.tableDisabledColor ? data.tableDisabledColor : "rgba(199, 199, 199, 0.6)";
        res.tableDisabledBColor = data.tableDisabledBColor ? data.tableDisabledBColor : "rgba(208, 208, 208, 1)";

        return res;
    }
}