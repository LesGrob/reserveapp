import { TimeTransform, WeekDay } from './enums.models';

export class DayInterval {
    id: number = 0;
    start: String = "12:00";
    end: String = "18:00";

    toJson() {
        return {
            id: this.id,
            start: TimeTransform.toMinutes(this.start),
            end: TimeTransform.toMinutes(this.end)
        }
    }

    static fromJson(data: any): DayInterval {
        if (data == null) return null;
        let res = new DayInterval();
        res.id = data.id;
        res.start = TimeTransform.transformMinutes(data.start);
        res.end = TimeTransform.transformMinutes(data.end);
        return res;
    }
}

export class ScheduleDay {
    id: number = 0;
    isActive: boolean = false;
    weekDay: WeekDay = 1;
    intervals: Array<DayInterval> = [];

    public constructor(weekDay: WeekDay = 1, intervals: Array<DayInterval> = []) {
        this.weekDay = weekDay;
        this.intervals = intervals;
    }

    toJson() {
        return {
            id: this.id,
            weekDay: this.weekDay,
            isActive: this.isActive,
            intervals: this.intervals.map(data => data.toJson())
        }
    }

    static fromJson(data: any): ScheduleDay {
        if (data == null) return null;
        let res = new ScheduleDay();
        res.id = data.id;
        res.weekDay = data.weekDay;
        res.isActive = data.isActive;
        res.intervals = data.intervals.map(data => DayInterval.fromJson(data));
        return res;
    }
}

export class Schedule {
    id: number = 0;
    reservationLength: String = "01:00";
    days: Array<ScheduleDay> = [];

    public constructor() {
        for (let i of [1, 2, 3, 4, 5, 6, 0])
            this.days.push(new ScheduleDay(i, [new DayInterval()]));
    }

    toJson() {
        return {
            id: this.id,
            reservationLength: TimeTransform.toMinutes(this.reservationLength),
            days: this.days.map(data => data.toJson())
        }
    }

    static fromJson(data: any): Schedule {
        if (data == null) return null;
        let res = new Schedule();
        res.id = data.id;
        res.reservationLength = TimeTransform.transformMinutes(data.reservationLength);
        res.days = data.days == null ? [] : data.days.map(data => ScheduleDay.fromJson(data)).sort((x, y) => {
            if (x.weekDay == 0) return 1;
            if (y.weekDay == 0) return -1;
            return x.weekDay > y.weekDay ? 1 : -1
        });
        return res;
    }
}