export class Company {
    id: number = 0;
    name: string = "";
    inn: string = "";
    ogrn: string = "";
    isIP: boolean = false;
    isConfirmed: boolean = false;

    toJson() {
        return {
            id: this.id,
            name: this.name,
            inn: this.inn,
            ogrn: this.ogrn,
            isIP: this.isIP
        }
    }

    static fromJson(data: any): Company {
        if (data == null) return null;
        let res = new Company();
        res.id = data.id;
        res.name = data.name;
        res.inn = data.inn;
        res.ogrn = data.ogrn;
        res.isIP = data.isIP;
        res.isConfirmed = data.isConfirmed;
        return res;
    }
}