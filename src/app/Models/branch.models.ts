import { Status } from './enums.models';
import { Company } from './company.models';
import { Bill } from './bill.models';
import { Tariff } from './tariff.models';
import { Schedule } from './schedule.models';
import { Zone } from './zone.models';
import { CompanyBot } from './bot.model';

export class Branch {
    id: number = 0;
    name: string = "";
    adress: string = "";
    phone: string = "";
    imagePath: string = "";
    company: Company = new Company();
    status: Status = 1;
    tariff: Tariff = new Tariff();
    bill: Bill = null;
    schedule: Schedule = new Schedule();
    zones: Array<Zone> = [];
    newZones: Array<Zone> = [];
    branchImage: any = "";
    branchImageName: string = "";
    telegramIsAvailiable: boolean = false;
    bot: CompanyBot = new CompanyBot();

    toJson() {
        return {
            id: this.id,
            name: this.name,
            adress: this.adress,
            phone: this.phone,
            company: this.company.toJson(),
            tariff: this.tariff.toJson(),
            schedule: this.schedule.toJson(),
            zones: this.zones.map(data => data.toJson()),
            newZones: this.newZones.map(data => data.toJson()),
            branchImage: this.branchImage,
            branchImageName: this.branchImageName,
            telegramIsAvailiable: this.telegramIsAvailiable
        }
    }

    clone(): Branch {
        let branch = new Branch();

        branch.id = this.id;
        branch.name = this.name;
        branch.adress = this.adress;
        branch.phone = this.phone;
        branch.imagePath = this.imagePath;
        branch.company = this.company;
        branch.status = this.status;
        branch.tariff = this.tariff;
        branch.schedule = this.schedule;
        branch.zones = Object.assign([], this.zones.map(x => x.clone()));
        branch.newZones = Object.assign([], this.newZones.map(x => x.clone()));
        branch.branchImage = this.branchImage;
        branch.branchImageName = this.branchImageName;
        branch.telegramIsAvailiable = this.telegramIsAvailiable;
        branch.bot = this.bot;

        return branch;
    }

    static fromJson(data: any): Branch {
        if (data == null) return null;
        let res = new Branch();
        res.id = data.id;
        res.name = data.name;
        res.adress = data.adress;
        res.phone = data.phone;
        res.imagePath = data.imagePath ? "/Resources/BranchImages/" + data.imagePath : data.imagePath;
        res.company = Company.fromJson(data.company);
        res.tariff = Tariff.fromJson(data.tariff);
        res.bill = Bill.fromJson(data.bill);
        res.schedule = Schedule.fromJson(data.schedule);
        res.status = data.status;
        res.zones = data.zones ? data.zones.map(data => Zone.fromJson(data)) : [];
        res.newZones = data.newZones ? data.newZones.map(data => Zone.fromJson(data)) : [];
        res.telegramIsAvailiable = data.telegramIsAvailiable;
        res.bot = CompanyBot.fromJson(data.bot);
        return res;
    }
}
