import { Tariff } from './tariff.models';
import { Branch } from './branch.models';
import { Status } from './enums.models';

export class Bill {
    id: number = 0;
    amount: number = 0;
    tableCount: number = 0;
    telegramIsAvailiable: boolean = false;
    date: Date = new Date();
    tariff: Tariff = new Tariff();
    branch: Branch = new Branch();
    status: Status = 1;

    toJson() {

        return {
            id: this.id,
            amount: this.amount,
            tableCount: this.tableCount,
            telegramIsAvailiable: this.telegramIsAvailiable,
            date: this.date,
            tariff: this.tariff.toJson(),
            branch: this.branch.toJson()
        }
    }

    static fromJson(data: any): Bill {
        if (data == null) return null;
        let res = new Bill();
        res.id = data.id;
        res.amount = data.amount;
        res.tableCount = data.tableCount;
        res.telegramIsAvailiable = data.telegramIsAvailiable;
        res.date = new Date(data.date);
        res.tariff = Tariff.fromJson(data.tariff);
        res.branch = Branch.fromJson(data.branch);
        res.status = data.status;
        return res;
    }
}