export class Tariff {
    id: number = 0;
    name: string = "";
    price: number = 0;
    tablePrice: number = 0;
    botPrice: number = 0;
    period: number = 0;
    isAvailiable: boolean = true;

    toJson() {
        return {
            id: this.id,
            name: this.name,
            price: this.price,
            tablePrice: this.tablePrice,
            botPrice: this.botPrice,
            period: this.period,
            isAvailiable: this.isAvailiable
        }
    }

    static fromJson(data: any): Tariff {
        if (data == null) return null;
        let res = new Tariff();
        res.id = data.id;
        res.name = data.name;
        res.price = data.price;
        res.tablePrice = data.tablePrice;
        res.botPrice = data.botPrice;
        res.period = data.period;
        res.isAvailiable = data.isAvailiable;
        return res;
    }
}
