import { Component, Inject, ViewChild } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { NgModel } from '@angular/forms';
import { Order } from 'src/app/Models/order.models';

@Component({
  selector: 'dialog-confirm-order',
  templateUrl: 'confirmOrder.dialog.html',
})
export class ConfirmOrderDialog {
  @ViewChild("phoneI", { static: false }) phoneI: NgModel;
  @ViewChild("emailI", { static: false }) emailI: NgModel;

  constructor(public dialogRef: MatDialogRef<ConfirmOrderDialog>, @Inject(MAT_DIALOG_DATA) public data: any) { }
  order: Order = new Order();
  onNoClick(): void { this.dialogRef.close(); }

  submitValid(): boolean {
    if (!this.emailI || this.emailI.invalid) return false;
    if (this.phoneI && this.phoneI.invalid) return false;
    if (this.order.name == null || this.order.name == "") return false;
    return true;
  }
}