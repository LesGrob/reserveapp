export { Component, NgModule } from "@angular/core";
export { BrowserModule } from "@angular/platform-browser";
export { BrowserAnimationsModule } from "@angular/platform-browser/animations";
export { HttpClientModule } from '@angular/common/http';
export { FormsModule, FormControl, FormGroup, Validators, FormBuilder, ReactiveFormsModule } from "@angular/forms";
export { Routes, RouterModule } from "@angular/router";

export {
    MatAutocompleteModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatDatepickerModule,
    MatDialogModule,
    MatExpansionModule,
    MatIconModule,
    MatSnackBarModule,
    MatProgressBarModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatSelectModule,
    MatSidenavModule,
    MatTabsModule,
    MatToolbarModule,
    MatBadgeModule,
    MatBottomSheetModule
} from "@angular/material";

//Custom components and modules
export * from "./app.component";
