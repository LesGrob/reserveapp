import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule, LOCALE_ID } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DatePipe } from '@angular/common';

import { ColorPickerModule } from 'ngx-color-picker';
import { HighlightJsModule } from 'ngx-highlight-js';
import { AngularDraggableModule } from 'angular2-draggable';
import {
  MatButtonModule, MatIconModule, MatInputModule, MatDialogModule, MatSelectModule,
  MatProgressSpinnerModule, MatDatepickerModule, MatTabsModule, MatNativeDateModule,
  MatSidenavModule, MatListModule, MatCardModule, MatToolbarModule, MatCheckboxModule,
  MatSnackBarModule, MatExpansionModule, MatTableModule, MatProgressBarModule,
  MatButtonToggleModule, MatMenuModule, MatSliderModule, DateAdapter
} from '@angular/material';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DragDropModule } from '@angular/cdk/drag-drop';
import localeRu from '@angular/common/locales/ru';
import { CalendarModule, DateAdapter as CalendarAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';

import { AppComponent } from 'src/app/app.component';//'@app/app.component';
import { IndexComponent } from 'src/app/Components/index/index.component';//'@app/app.component';
import { CanvasComponent } from 'src/app/Components/branch/canvas/canvas.component';//'@app/Components/canvas.component';
import { DisabledCanvasComponent } from 'src/app/Components/branch/disabled_canvas/disabled_canvas.component';
import { ClientComponent } from 'src/app/Components/client/client.component';
import { ProfileComponent } from 'src/app/Components/profile/profile.component';
import { BranchComponent } from 'src/app/Components/branch/branch.component';
import { ScheduleComponent } from 'src/app/Components/branch/schedule/schedule.component';
import { TariffComponent } from 'src/app/Components/branch/tariff/tariff.component';
import { CalendarComponent } from 'src/app/Components/calendar/calendar.component';

import { SharedService, SoftDeletePipe } from 'src/app/Services/shared.service';
import { ConfirmOrderDialog } from 'src/app/Dialogs/confirmOrder/confirmOrder.dialog';

import { AuthComponent } from "src/app/Components/authorization/auth.component";
import { LogInComponent } from "src/app/Components/authorization/logInComponent/LogIn.component";
import { RegistrationComponent } from "src/app/Components/authorization/registrationComponent/Registration.component";
import { ResetPassword } from "src/app/Components/authorization/resetPasswordComponent/ResetPassword.component";

import { AuthGuard, AuthService } from 'src/app/Services/authentication.service';
import { HttpService } from 'src/app/Services/http.service';

import { Dialog, DialogInfoComponent, DialogComponent, DialogCanvasComponent, DialogConfirmComponent } from 'src/app/Templates/Dialogs/Dialog';
import { WeekDayComponent } from 'src/app/Templates/WeekDay/weekDay.component';

import { OGRNvalidDirective, INNvalidDirective, PhoneValidDirective, EmailValidDirective } from './CustomValidators';

import { HttpClientModule } from '@angular/common/http';
import { registerLocaleData, APP_BASE_HREF } from '@angular/common';
import { CustomDateAdapter } from './dateadapter';

registerLocaleData(localeRu);

@NgModule({
  declarations: [
    AppComponent, IndexComponent, CanvasComponent, DisabledCanvasComponent, BranchComponent, ClientComponent,
    ProfileComponent, ScheduleComponent, TariffComponent, CalendarComponent,
    AuthComponent, LogInComponent, RegistrationComponent, ResetPassword,
    ConfirmOrderDialog,
    WeekDayComponent,
    SoftDeletePipe,
    DialogConfirmComponent, DialogInfoComponent, DialogComponent, DialogCanvasComponent,
    OGRNvalidDirective, INNvalidDirective, PhoneValidDirective, EmailValidDirective
  ],
  imports: [BrowserModule, FormsModule, BrowserAnimationsModule, HttpClientModule,
    MatButtonModule, MatIconModule, MatInputModule, MatDialogModule, MatSelectModule, MatProgressSpinnerModule,
    MatDatepickerModule, MatTabsModule, MatNativeDateModule, MatSidenavModule, MatListModule, MatCardModule,
    MatToolbarModule, MatCheckboxModule, MatSnackBarModule, MatExpansionModule, MatTableModule, MatProgressBarModule,
    MatButtonToggleModule, MatMenuModule, MatSliderModule,
    ColorPickerModule,
    ReactiveFormsModule, FormsModule,
    DragDropModule, HighlightJsModule, AngularDraggableModule,
    CalendarModule.forRoot({
      provide: CalendarAdapter,
      useFactory: adapterFactory
    }),
    RouterModule.forRoot([
      { path: "", redirectTo: "Service", pathMatch: "full" },
      {
        path: 'Service', canActivate: [AuthGuard], component: IndexComponent, children: [
          { path: "Info/:id", component: BranchComponent },
          { path: "Calendar/:id", component: CalendarComponent }
        ]
      },
      { path: 'app/:id', component: ClientComponent },
      {
        path: 'Auth', component: AuthComponent, children: [
          { path: "LogIn", component: LogInComponent },
          { path: "Registration", component: RegistrationComponent },
          { path: "ResetPassword", component: ResetPassword }
        ]
      },
      { path: 'LogIn', redirectTo: '/Auth/LogIn', pathMatch: 'full' },
      { path: 'Registration', redirectTo: '/Auth/Registration', pathMatch: 'full' },
      { path: 'ResetPassword', redirectTo: '/Auth/ResetPassword', pathMatch: 'full' },
    ])
  ],
  entryComponents: [ConfirmOrderDialog, DialogConfirmComponent, DialogInfoComponent, DialogComponent, DialogCanvasComponent],
  providers: [SharedService, HttpService, AuthGuard, AuthService, Dialog, HttpClientModule, DatePipe,
    { provide: LOCALE_ID, useValue: 'ru-RU' }, { provide: DateAdapter, useClass: CustomDateAdapter }],
  bootstrap: [AppComponent]
})
export class AppModule { }