import { throwError as observableThrowError, Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams, HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';
import { Router } from '@angular/router';
import { Dialog } from "../Templates/Dialogs/Dialog"

@Injectable()
export class HttpService {

    constructor(private router: Router, private http: HttpClient, public dialog: Dialog) { }

    public loading: boolean = false;

    private stringifyData(data: Array<any>) {
        let _data: string = "/";
        for (let item of data)
            _data += (item + "/");
        return _data;
    }

    //Временные экземпляры с JWT Tokens - полный перекат с прошлых на эти методы с JWT
    postData(_controller: string, _method: string, data: any, parameters?: HttpParams): Observable<object> {
        this.loading = true;
        let headers = new HttpHeaders().set('Content-Type', 'application/json');
        return this.http.post('/api/' + _controller + '/' + _method, data, { headers: this.JWTHeader(headers) }).pipe(
            catchError(error => {
                this.loading = false;
                return this.catchError(error);
            }),
            map(data => {
                return this.catchApiResponse(data);
            })
        );
    }

    getData(_controller: string, _method: string, _data: Array<any>, parameters?: Array<any>): Observable<object> {
        this.loading = true;
        return this.http.get('/api/' + _controller + '/' + _method + this.stringifyData(_data), { headers: this.JWTHeader(), params: this.HandleParameter(parameters) })
            .pipe(
                catchError(error => {
                    this.loading = false;
                    return this.catchError(error);
                }),
                map(data => {
                    return this.catchApiResponse(data);
                })
            );
    }

    private catchError(error: any): Observable<any> {
        if (error.status === 401 || error.status === 403) {
            this.router.navigate(['/LogIn']);
        } else {
            this.dialog.openDialog("Ошибка (" + error.status + ")", (error.error ? error.error.message : error.message));
        }
        return observableThrowError(new Error(error.status));
    }

    private catchApiResponse(data: any): any {
        if (data.code === 401 || data.code === 403) {
            this.loading = false;
            this.router.navigate(['/LogIn']);
        } else if (data.code != 200) {
            this.loading = false;
            this.dialog.openDialog("Ошибка (" + data.code + ")", (data.message));
            throw new Error(data.code);
        }
        return data.data;
    }


    //Добавление заголовка для аутентификации JWT Tokens
    private JWTHeader(headers?: HttpHeaders): HttpHeaders {
        //headers - Остальные заголовки, к которым нужно добавить заголовок аутентификации, [если есть]
        //create JWT header
        let token = localStorage.getItem("JWTtoken");
        if (headers) {
            headers = headers.append('Authorization', 'Bearer ' + token);
            return headers;
        }
        if (token) {
            return new HttpHeaders().set('Authorization', 'Bearer ' + token);
        }
    }

    //Формирование параметров для запроса
    private HandleParameter(parameters?: Array<any>): HttpParams {
        let params = new HttpParams();
        if (!parameters || parameters.length == 0) return;
        for (let parameter of parameters)
            if (parameter) params = params.append(parameter.name, parameter.value);
        return params;
    }
}