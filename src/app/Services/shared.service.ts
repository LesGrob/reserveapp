import { Injectable } from '@angular/core';
import { MatSnackBar } from '@angular/material';
import { HttpService } from './http.service';
import { MediaObserver, MediaChange } from '@angular/flex-layout'
import { Pipe, PipeTransform } from '@angular/core';

@Injectable()
export class SharedService {

    constructor(private media$: MediaObserver, private snackBar: MatSnackBar, public httpService: HttpService) {
        this.appUrl = window.location.origin;
        this.media$.media$.subscribe((change: MediaChange) => {
            if (change.mqAlias === 'xs' || change.mqAlias === 'gt-xs' || change.mqAlias === 'sm')
                this.isDesktop = false;
            else
                this.isDesktop = true;
        });
    }

    public isDesktop = true;
    public isConfirmed = false;
    public appUrl = "";

    public openSnackBar(message: string, alert: string) {
        this.snackBar.open(message, alert, {
            duration: 2000,
        });
    }
}


@Pipe({
    name: 'softdeleted',
    pure: false
})
export class SoftDeletePipe implements PipeTransform {
    transform(items: any[]): any {
        if (!items) {
            return items;
        }
        return items.filter(item => item.deleted == false);
    }
}