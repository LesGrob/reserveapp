import { Injectable } from '@angular/core';
import { CanActivate, Router, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { HttpService } from "./http.service";
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class AuthService {
    constructor(private router: Router, public httpService: HttpService) { }

    login(UserData: any) {
        this.httpService.postData('Users', 'LogIn', UserData).pipe(map(data => data as any)).subscribe(
            data => {
                this.httpService.loading = false;
                localStorage.setItem("JWTtoken", data.token as any);
                this.router.navigate(["/"]);
            });
    }

    logout(): void {
        this.httpService.postData("Users", "LogOut", []).subscribe(data => {
            this.httpService.loading = false;
        });
        localStorage.clear();
        sessionStorage.clear();
    }
}

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private router: Router) { }
    canActivate(
        next: ActivatedRouteSnapshot,
        state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean | UrlTree {
        return new Observable<boolean>(observer => {
            if (localStorage.getItem("JWTtoken"))
                return observer.next(true);
            this.router.navigate(["/LogIn"]);
            return observer.next(false);
        })
    }
}
