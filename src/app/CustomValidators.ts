import { AbstractControl, ValidatorFn, FormGroup, NG_VALIDATORS, Validator } from "@angular/forms"
import { Directive, Input } from "@angular/core";

export class CustomValidation {
    static IsMatchPasswords(AC: AbstractControl) {
        let password = AC.get('password').value;
        let confirmPassword = AC.get('confirmPassword').value;
        return password !== confirmPassword ? AC.get('confirmPassword').setErrors({ MatchPassword: true }) : null;
    }
}

export function IsValidEmail(AC: AbstractControl) {
    let emailRegex = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
    return emailRegex.test(AC.value) ? null : { ErrorEmail: true };
}

@Directive({
    selector: '[phoneValid]',
    providers: [{ provide: NG_VALIDATORS, useExisting: PhoneValidDirective, multi: true }]
})
export class PhoneValidDirective implements Validator {
    validate(AC: AbstractControl) {
        let phoneRegex = new RegExp(/^$|^((\+7|7|8)+([0-9]){10})$/);
        return phoneRegex.test(AC.value) ? null : { 'phoneValid': false };
    }
}

@Directive({
    selector: '[emailValid]',
    providers: [{ provide: NG_VALIDATORS, useExisting: EmailValidDirective, multi: true }]
})
export class EmailValidDirective implements Validator {
    validate(AC: AbstractControl) {
        let emailRegex = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);
        return emailRegex.test(AC.value) ? null : { 'emailValid': false };
    }
}

@Directive({
    selector: '[innValid]',
    providers: [{ provide: NG_VALIDATORS, useExisting: INNvalidDirective, multi: true }]
})
export class INNvalidDirective implements Validator {
    validate(AC: AbstractControl): { [key: string]: any } | null {
        if (AC.value == null || AC.value == "" || AC.value.match(/\D/)) {
            //alert("Введённый ИНН пустой или не является числом");
            return { 'innValid': false };
        }
        // проверка на 10 и 12 цифр
        if (AC.value.length != 12 && AC.value.length != 10) {
            return { 'innValid': false };
        }
        // проверка по контрольным цифрам
        if (AC.value.length == 10) {
            var dgt10 = String(((
                2 * AC.value[0] + 4 * AC.value[1] + 10 * AC.value[2] +
                3 * AC.value[3] + 5 * AC.value[4] + 9 * AC.value[5] +
                4 * AC.value[6] + 6 * AC.value[7] + 8 * AC.value[8]) % 11) % 10);
            if (AC.value[9] == dgt10) {
                return null
            } else {
                return { 'innValid': false };
            }
        }
        if (AC.value.length == 12) {
            var dgt11 = String(((
                7 * AC.value[0] + 2 * AC.value[1] + 4 * AC.value[2] +
                10 * AC.value[3] + 3 * AC.value[4] + 5 * AC.value[5] +
                9 * AC.value[6] + 4 * AC.value[7] + 6 * AC.value[8] +
                8 * AC.value[9]) % 11) % 10);
            var dgt12 = String(((
                3 * AC.value[0] + 7 * AC.value[1] + 2 * AC.value[2] +
                4 * AC.value[3] + 10 * AC.value[4] + 3 * AC.value[5] +
                5 * AC.value[6] + 9 * AC.value[7] + 4 * AC.value[8] +
                6 * AC.value[9] + 8 * AC.value[10]) % 11) % 10);
            if (AC.value[10] == dgt11 && AC.value[11] == dgt12) {
                return null
            } else {
                return { 'innValid': false };
            }
        }
        return { 'innValid': false };
    }
}

@Directive({
    selector: '[ogrnValid]',
    providers: [{ provide: NG_VALIDATORS, useExisting: OGRNvalidDirective, multi: true }]
})
export class OGRNvalidDirective implements Validator {
    validate(AC: AbstractControl): { [key: string]: any } | null {
        if (AC.value == null || AC.value == "") {
            return null;
        }
        var dgt13;
        // проверка на число
        if (AC.value.match(/\D/)) {
            //alert("Введённый ОГРН не является числом");
            return { 'ogrnValid': false };
        }
        // проверка на 13 и 15 цифр
        if (AC.value.length != 13 && AC.value.length != 15) {
            return { 'ogrnValid': false };
        }
        // проверка ОГРН для ЮЛ
        if (AC.value.length == 13) {
            // проверка по контрольным цифрам
            var num12 = AC.value;
            num12 = Math.floor((num12 / 10) % 11);
            if (num12 == 10) { dgt13 = 0; }
            else { dgt13 = num12; }
            if (AC.value[12] == dgt13) {
                return null
            } else {
                return { 'ogrnValid': false };
            }
        }
        // проверка ОГРН для ИП
        if (AC.value.length == 15) {
            // проверка по контрольным цифрам
            var num14 = AC.value;
            num14 = Math.floor((num14 / 10) % 13);
            var dgt15 = num14 % 10;
            if (AC.value[14] == dgt15) {
                return null
            } else {
                return { 'ogrnValid': false };
            }
        }
        return null;
    }
}