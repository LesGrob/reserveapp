import { Component, ElementRef, ViewChild, AfterContentInit, Input, TemplateRef } from '@angular/core';
import { CdkDragEnd } from '@angular/cdk/drag-drop';
import { Dialog } from 'src/app/Templates/Dialogs/Dialog';
import { SharedService } from 'src/app/Services/shared.service';
import { Zone } from 'src/app/Models/zone.models';
import { Table } from 'src/app/Models/table.models';

@Component({
    selector: 'canvas-main',
    templateUrl: './canvas.component.html',
    styleUrls: ['./canvas.component.css']
})

export class CanvasComponent implements AfterContentInit {
    @ViewChild('planImage', { static: false }) planImage: ElementRef;

    get hasData(): boolean { return this.zones && this.zones.length > 0; }
    @Input() zones: Array<Zone> = [];

    tableStatus: number = 0;
    zoneIndex: number = 0;
    selectedIndex: number = null;
    expandedTablePanel: boolean = false;
    koeff = 1;
    s_point(pointX: number, pointY: number): { x: number, y: number } { return { x: pointX * this.koeff, y: pointY * this.koeff }; }
    s_length(length: number): number { return length * this.koeff; }

    constructor(public dialog: Dialog, public sharedService: SharedService) { }
    ngAfterContentInit() {
        window.onresize = (e) => {
            this.imageResize();
        }
    }

    //file load
    onBackgroundClick() { this.selectedIndex = null; }
    handleFileInput(event: any) {
        if (event.target.files.length > 0) {
            let file = event.target.files[0];
            let fileType = file['type'];
            let validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
            if (validImageTypes.includes(fileType) && file.size < (1024 * 1024 * 10)) {
                let reader = new FileReader();
                let image = new Image();
                image.onload = (e: any) => {
                    this.zones[this.zoneIndex].imageWidth = e.target.width;
                    this.zones[this.zoneIndex].imageHeight = e.target.height;
                    this.zones[this.zoneIndex].imagePath = e.target.src;
                }
                reader.onload = (e: any) => {
                    let data = e.target;
                    var base64result = data.result.substr(data.result.indexOf(',') + 1);
                    if (this.zones.length < 1) {
                        this.zones = [new Zone()];
                        this.zoneIndex = 0;
                    }
                    this.selectedIndex = null;
                    this.zones[this.zoneIndex].zoneImage = base64result;
                    this.zones[this.zoneIndex].zoneImageName = file.name;
                    this.zones[this.zoneIndex].tables = [];
                    image.src = e.target.result;
                }
                reader.readAsDataURL(file);
            }
        }
    }

    //Table funcs
    addTable() {
        if (this.zones) {
            let table = new Table("Стол #", 10, 10);
            this.zones[this.zoneIndex].tables.push(table);
        }
    }
    removeTable() {
        if (this.zones && this.selectedIndex != null) {
            if (this.zones[this.zoneIndex].tables.length > 1) {
                this.zones[this.zoneIndex].tables.splice(this.selectedIndex, 1);
            } else {
                this.zones[this.zoneIndex].tables = [];
            }
            this.selectedIndex = null;
        }
        this.dialog.close();
    }
    selectTable(index: number) {
        if (index >= 0 && index < this.zones[this.zoneIndex].tables.length) {
            this.selectedIndex = index;
        }
    }
    changeTable(value: any, type: string) {
        if (String(value) == String(Number(value)) && this.selectedIndex != null) {
            switch (type) {
                case 'd': this.zones[this.zoneIndex].tables[this.selectedIndex].depositCost = Number(value); break;
                case 's': if (Number(value) > 0) this.zones[this.zoneIndex].tables[this.selectedIndex].sits = Number(value); break;
                default: break;
            }
        }
    }

    dragEnded(event: CdkDragEnd, index: number) {
        if (this.zones) {
            let transform: String = event.source.element.nativeElement.style.transform;
            let arr = transform.replace(/translate3d/g, "").replace(/,/g, "").replace(/[()]/g, "").replace(/\s/g, "");
            let array = arr.split("px");
            if (array.length > 3) {
                this.zones[this.zoneIndex].tables[index].positionX = (Number(array[0]) + Number(array[3])) / this.koeff;
                this.zones[this.zoneIndex].tables[index].positionY = (Number(array[1]) + Number(array[4])) / this.koeff;
            } else if (array.length > 0) {
                this.zones[this.zoneIndex].tables[index].positionX = Number(array[0]) / this.koeff;
                this.zones[this.zoneIndex].tables[index].positionY = Number(array[1]) / this.koeff;
            }
        }
    }
    onResizeStart($event, i) { this.selectedIndex = i; }
    onResizeStop(event) {
        this.zones[this.zoneIndex].tables[this.selectedIndex].width = event.size.width / this.koeff;
        this.zones[this.zoneIndex].tables[this.selectedIndex].height = event.size.height / this.koeff;
    }

    //Zone funcs
    addZone() {
        this.zones.push(new Zone);
        this.zoneIndex = this.zones.length - 1;
    }
    removeZone() {
        if (this.zones.length > 1 && this.zoneIndex < this.zones.length) {
            let zone = this.zones[this.zoneIndex];
            this.dialog.openDialog("Удаление зоны", "Вы уверены, что хотите удалить '" + zone.name + "'?", true).subscribe(response => {
                if (response == true) {
                    this.zones.splice(this.zoneIndex, 1);
                    this.zones = this.zones;
                    this.zoneIndex = this.zones.length - 1;
                }
            });
        }
    }
    zoneChange(event) {
        if (event.index != null && event.index >= 0 && event.index < this.zones.length) {
            this.zoneIndex = event.index;
        }
    }



    //On image change
    onImgLoad() {
        this.expandedTablePanel = false;
        this.koeff = 1;
        this.selectedIndex = null;
        this.imageResize();
    }
    imageResize() {
        if (this.planImage && this.planImage.nativeElement) {
            let wK = this.planImage.nativeElement.clientWidth / this.zones[this.zoneIndex].imageWidth;
            let hK = this.planImage.nativeElement.clientHeight / this.zones[this.zoneIndex].imageHeight;
            this.koeff = Math.min(hK, wK);
        }
    }



    //Styles
    getTableStyle(index: number) {
        if (this.zones[this.zoneIndex] != null && index >= 0 && index < this.zones[this.zoneIndex].tables.length) {
            let item = this.zones[this.zoneIndex].tables[index];
            let point = this.s_point(item.positionX, item.positionY);

            let color;
            let bColor;

            switch (this.tableStatus) {
                case 1:
                    color = this.zones[this.zoneIndex].tableActiveColor;
                    bColor = this.zones[this.zoneIndex].tableActiveBColor;
                    break;
                case 2:
                    color = this.zones[this.zoneIndex].tableDisabledColor;
                    bColor = this.zones[this.zoneIndex].tableDisabledBColor;
                    break;
                default:
                    color = this.zones[this.zoneIndex].tableColor;
                    bColor = this.zones[this.zoneIndex].tableBColor;
                    break;
            }

            return {
                'transform': 'translate3d( ' + point.x + 'px, ' + point.y + 'px, 0px)',
                'width': this.s_length(item.width) + 'px',
                'height': this.s_length(item.height) + 'px',
                'border-radius': item.radius + 'px',
                'background-color': color,
                'border-color': bColor,
                'color': bColor
            };
        }
        return {};
    }

    menuStyle(status) {
        if (status == this.tableStatus)
            return {
                'color': 'white',
                'background-color': '#903449'
            }
        return {
            'color': '#222222',
            'background-color': '#ffffff'
        };
    }

    changeTableColor(status) {
        this.tableStatus = status;
    }



    //Modals
    @ViewChild('DTableSettings', { static: false }) DTableSettings: TemplateRef<any>;
    onTableChange() {
        if (this.zones[this.zoneIndex] && this.selectedIndex >= 0) {
            this.dialog.openDialogTemplate(this.DTableSettings, "300px");
        }
    }

    @ViewChild('DZoneName', { static: false }) DZoneName: TemplateRef<any>;
    onZoneRename() {
        if (this.zones[this.zoneIndex] && this.selectedIndex >= 0) {
            this.dialog.openDialogTemplate(this.DZoneName, "300px");
        }
    }
}