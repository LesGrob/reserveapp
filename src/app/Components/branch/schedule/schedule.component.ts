import { Component, Input, ViewChild, TemplateRef } from '@angular/core';
import { HttpService } from 'src/app/Services/http.service';
import { map } from 'rxjs/operators';
import { Dialog } from 'src/app/Templates/Dialogs/Dialog';
import { Branch } from 'src/app/Models/branch.models';
import { DisabledTime } from 'src/app/Models/disabledTime.models';
import { WeekDay } from 'src/app/Models/enums.models';
import { DayInterval } from 'src/app/Models/schedule.models';
import { SharedService } from 'src/app/Services/shared.service';

@Component({
    selector: 'schedule-component',
    templateUrl: './schedule.component.html',
    styleUrls: ['./schedule.component.css']
})
export class ScheduleComponent {
    branch: Branch = null;
    @Input() set branchProp(branch: Branch) {
        this.branch = branch;
        this.updateBranch();
    }

    disabledTimes: Array<DisabledTime> = [];
    newTime: DisabledTime = new DisabledTime();
    dayIndex: number = 0;

    constructor(public sharedService: SharedService, private httpService: HttpService, public dialog: Dialog) { }

    updateBranch() {
        if (this.branch && this.branch.id && this.branch.id != 0) {
            this.httpService.getData("DisabledTime", "DisabledTime", [this.branch.id]).pipe(map(data => data as Array<any>)).subscribe(data => {
                this.disabledTimes = this.getTimeList(data);
                this.httpService.loading = false;
            });
        }
    }

    AddDisabledTime() {
        if (this.branch.id && this.branch.id != 0) {
            this.newTime.branch = this.branch;
            let dtime = this.newTime.toJson();
            this.httpService.postData("DisabledTime", "AddDisabledTime", dtime).pipe(map(data => data as Array<any>)).subscribe(data => {
                this.disabledTimes = this.getTimeList(data);
                this.httpService.loading = false;
            });
        }
    }

    RemoveTime(timeId: number) {
        if (this.branch.id && this.branch.id != 0) {
            this.httpService.postData("DisabledTime", "RemoveDisabledTime/" + timeId, {}).pipe(map(data => data as Array<any>)).subscribe(data => {
                this.disabledTimes = this.getTimeList(data);
                this.httpService.loading = false;
            });
        }
    }

    getTimeList(data: Array<any>): Array<DisabledTime> {
        let times: Array<DisabledTime> = [];
        data.forEach(element => { times.push(DisabledTime.fromJson(element)); });
        return times;
    }

    getWeekDay(index: number) {
        return WeekDay[index];
    }

    selectDay(index: number) {
        this.dayIndex = index;
    }

    AddInterval(start, end) {
        let interval = new DayInterval();
        interval.start = start;
        interval.end = end;
        this.branch.schedule.days[this.dayIndex].intervals = this.branch.schedule.days[this.dayIndex].intervals.concat([interval]);

    }

    RemoveInterval(interval: DayInterval) {
        if (this.branch.schedule.days[this.dayIndex] == null ||
            this.branch.schedule.days[this.dayIndex].intervals == null ||
            this.branch.schedule.days[this.dayIndex].intervals.length < 1)
            return;
        this.branch.schedule.days[this.dayIndex].intervals = this.branch.schedule.days[this.dayIndex].intervals.filter(x => x != interval);
    }

    @ViewChild('DDisabledTime', { static: false }) DDisabledTime: TemplateRef<any>;
    openDisabledTime() {
        this.dialog.openDialogTemplate(this.DDisabledTime, "500px");
    }

    @ViewChild('DAddInterval', { static: false }) DAddInterval: TemplateRef<any>;
    openAddInterval() {
        this.dialog.openDialogTemplate(this.DAddInterval, "350px");
    }
}