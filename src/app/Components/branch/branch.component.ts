import { Component, OnInit, OnDestroy, ViewChild, TemplateRef } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { map } from 'rxjs/operators';
import { Subscription } from 'rxjs';
import { SharedService } from 'src/app/Services/shared.service';
import { Dialog } from 'src/app/Templates/Dialogs/Dialog';
import { NgModel } from '@angular/forms';
import { Tariff } from 'src/app/Models/tariff.models';
import { Branch } from 'src/app/Models/branch.models';
import { Bill } from 'src/app/Models/bill.models';
import { Zone } from 'src/app/Models/zone.models';

@Component({
    selector: 'branch-component',
    templateUrl: './branch.component.html',
    styleUrls: ['./branch.component.css']
})
export class BranchComponent implements OnInit, OnDestroy {
    tariffs: Array<Tariff> = [];
    branchId: number = 0;
    branch: Branch = null;
    private step = 0;

    get pluginCode(): string {
        return `<iframe width="560" height="315" src="` + window.location.origin + "/app/" + this.branchId + `"></iframe>`;
    }

    @ViewChild("phoneI", { static: false }) phoneI: NgModel;

    private routeSubscribtion: Subscription;
    constructor(private router: Router, private route: ActivatedRoute,
        public sharedService: SharedService, private dialog: Dialog) { }

    ngOnDestroy() { this.routeSubscribtion.unsubscribe(); }
    ngOnInit() {
        this.sharedService.httpService.loading = true;
        this.routeSubscribtion = this.route.params.subscribe(data => {
            this.branchId = data.id;
            if (data.id == 0) {
                this.branch = new Branch();
            } else {
                this.sharedService.httpService.getData("Branches", "Branch", [this.branchId]).subscribe(data => {
                    this.sharedService.httpService.loading = false;
                    this.branch = Branch.fromJson(data);
                })
            }
            this.sharedService.httpService.getData("Branches", "Tariffs", []).pipe(map(data => data as Array<any>)).subscribe(data => {
                this.tariffs = data.map(data => Tariff.fromJson(data));
                this.sharedService.httpService.loading = false;
            })
        })
    }

    saveData() {
        if (!this.sharedService.isConfirmed) {
            this.dialog.openDialog("Уведомление",
                "Чтобы добавить заведение необходимо зарегестрировать компанию на вкладке 'Профиль'", true)
        } else if (this.submitValid()) {
            this.sharedService.httpService.loading = true;
            let br = this.branch.toJson();
            this.sharedService.httpService.postData("Branches", "Branch", br).pipe(map(data => Branch.fromJson(data))).subscribe(data => {
                this.sharedService.httpService.loading = false;
                if (this.branchId == 0) {
                    this.router.navigate(['/Service/Info', data.id]);
                } else {
                    this.branch = data;
                }
                this.sharedService.openSnackBar("Заведение сохранено", "ОК");
            })
        }
    }

    @ViewChild('DNewPlugin', { static: false }) DNewPlugin: TemplateRef<any>;
    addNewZones() {
        if (this.branch.newZones && this.branch.newZones.length == 0) {
            // let clone = this.branch.clone();
            // this.branch.newZones = clone.zones;
            this.branch.newZones = [new Zone()]
        }
        this.dialog.openDialogCanvasTemplate(this.DNewPlugin);
    }
    removeNewZones() {
        this.branch.newZones = [];
    }

    pay() {
        if (this.branchId != 0) {
            this.dialog.openDialog(this.branch.name, "Оплатить?", true)
                .subscribe(result => {
                    if (result == true) {
                        let bill = new Bill();
                        bill.branch = this.branch;
                        bill.tariff = this.branch.tariff;
                        bill.amount = this.totalSum();
                        bill.tableCount = this.tablesCount();
                        bill.telegramIsAvailiable = this.branch.telegramIsAvailiable;
                        this.sharedService.httpService.loading = true;
                        this.sharedService.httpService.postData("Payment", "Pay", bill.toJson()).pipe(map(data => Branch.fromJson(data))).subscribe(data => {
                            this.branch = data;
                            this.sharedService.httpService.loading = false;
                        });
                    }
                })
        }
    }

    prolong() {
        if (this.branchId != 0) {
            this.dialog.openDialog(this.branch.name, "Продлить?", true)
                .subscribe(result => {
                    if (result == true) {
                        let bill = new Bill();
                        bill.branch = this.branch;
                        bill.tariff = this.branch.tariff;
                        this.sharedService.httpService.loading = true;
                        this.sharedService.httpService.postData("Payment", "Prolong", bill.toJson()).pipe(map(data => Branch.fromJson(data))).subscribe(data => {
                            this.branch = data;
                            this.sharedService.httpService.loading = false;
                        });
                    }
                })
        }
    }

    submitValid(): boolean {
        if (!this.phoneI || this.phoneI.invalid) return false;
        if (!this.branch || !this.branch.tariff || this.branch.tariff.id < 1) return false;
        for (var f of this.branch.zones) {
            if (!f.imageWidth || !f.imageHeight || f.imageHeight == 0 || f.imageWidth == 0 || f.tables.length < 1)
                return false;
        }
        for (var f of this.branch.newZones) {
            if (!f.imageWidth || !f.imageHeight || f.imageHeight == 0 || f.imageWidth == 0 || f.tables.length < 1)
                return false;
        }
        let res = this.branch.name && this.branch.name != "" && this.branch.zones.length > 0;
        return res;
    }

    removeBranch() {
        if (this.branchId != 0) {
            this.dialog.openDialog(this.branch.name, "Вы действительно хотите удалить филиал? После удаления он перестанет быть доступным для бронирования!", true)
                .subscribe(result => {
                    if (result == true) {
                        this.sharedService.httpService.loading = true;
                        this.sharedService.httpService.postData("Branches", "RemoveBranch/" + this.branchId, {}).subscribe(data => {
                            this.sharedService.httpService.loading = false;
                            this.router.navigate(['/Service']);
                        })
                    }
                })
        }
    }

    handleFileInput(event: any) {
        if (event.target.files.length > 0) {
            let file = event.target.files[0];
            let fileType = file['type'];
            let validImageTypes = ['image/gif', 'image/jpeg', 'image/png'];
            if (validImageTypes.includes(fileType) && file.size < (1024 * 1024 * 10)) {
                let reader = new FileReader();
                reader.onload = (e: any) => {
                    let data = e.target;
                    var base64result = data.result.substr(data.result.indexOf(',') + 1);
                    this.branch.branchImage = base64result;
                    this.branch.branchImageName = file.name;
                    this.branch.imagePath = e.target.result;
                }
                reader.readAsDataURL(file);
            }
        }
    }

    tablesCount(): number {
        let sum = 0;
        if (this.branch && this.branch.zones) {
            this.branch.zones.forEach(element => {
                sum += element.tables.length;
            });
        }
        return sum;
    }
    totalSum(): number {
        if (!this.branch || !this.branch.tariff) return 0;
        return (this.branch.tariff.price + this.tablesCount() * this.branch.tariff.tablePrice) * this.branch.tariff.period +
            (this.branch.telegramIsAvailiable ? this.branch.tariff.botPrice : 0);
    }


    scrollTo(elementId: string) {
        var element = document.getElementById(elementId);
        document.getElementById("branchContainer").scrollLeft = element.offsetLeft;
    }
}