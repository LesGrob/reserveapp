import { Component, Input, ElementRef, ViewChild, AfterContentInit } from '@angular/core';
import { Branch } from 'src/app/Models/branch.models';

@Component({
    selector: 'disabled-canvas',
    templateUrl: './disabled_canvas.component.html',
    styleUrls: ['./disabled_canvas.component.css']
})

export class DisabledCanvasComponent implements AfterContentInit {
    @ViewChild('planImage', { static: false }) planImage: ElementRef;
    get hasData(): boolean { return this.branch && this.branch.zones.length > 0; }
    @Input() branch: Branch = null;
    zoneIndex: number = 0;
    koeff = 1;

    s_point(pointX: number, pointY: number): { x: number, y: number } { return { x: pointX * this.koeff, y: pointY * this.koeff }; }
    s_length(length: number): number { return length * this.koeff; }

    zoneChange(event) {
        if (event.index >= 0 && event.index < this.branch.zones.length) {
            this.zoneIndex = event.index;
        }
    }

    ngAfterContentInit() {
        window.onresize = (e) => {
            this.imageResize();
        }
    }

    //Image handlers
    imageResize() {
        this.koeff = 1;
        if (this.planImage) {
            let wK = this.planImage.nativeElement.clientWidth / this.branch.zones[this.zoneIndex].imageWidth;
            let hK = this.planImage.nativeElement.clientHeight / this.branch.zones[this.zoneIndex].imageHeight;
            this.koeff = Math.min(hK, wK);
        }
    }

    //Styles
    getTableStyle(index: number) {
        if (this.branch.zones[this.zoneIndex] != null && index >= 0 && index < this.branch.zones[this.zoneIndex].tables.length) {
            let item = this.branch.zones[this.zoneIndex].tables[index];
            let point = this.s_point(item.positionX, item.positionY);

            let color = this.branch.zones[this.zoneIndex].tableColor;
            let bColor = this.branch.zones[this.zoneIndex].tableBColor;

            return {
                'transform': 'translate3d( ' + point.x + 'px, ' + point.y + 'px, 0px)',
                'width': this.s_length(item.width) + 'px',
                'height': this.s_length(item.height) + 'px',
                'border-radius': item.radius + 'px',
                'background-color': color,
                'border-color': bColor,
                'color': bColor
            };
        }
        return {};
    }
}