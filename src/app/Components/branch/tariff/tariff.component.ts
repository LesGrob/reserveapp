import { Component, Input, EventEmitter, Output } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Branch } from 'src/app/Models/branch.models';
import { Tariff } from 'src/app/Models/tariff.models';
import { StatusDescription } from 'src/app/Models/enums.models';

@Component({
    selector: 'tariff-component',
    templateUrl: './tariff.component.html',
    styleUrls: ['./tariff.component.css']
})
export class TariffComponent {
    branch: Branch = null;
    tariffId: number;
    tariffs: Array<Tariff> = [];

    constructor(private datePipe: DatePipe) { }

    @Input() set branchProp(branch: Branch) {
        this.tariffId = branch.tariff.id;
        this.branch = branch;
    }

    @Input() set tariffsProp(tariffs: Array<Tariff>) {
        this.tariffs = tariffs;
    }

    @Output() payChange = new EventEmitter();
    @Output() prolongChange = new EventEmitter();

    tablesCount(): number {
        let sum = 0;
        if (this.branch && this.branch.zones) {
            this.branch.zones.forEach(element => {
                sum += element.tables.length;
            });
        }
        return sum;
    }

    newTablesCount(): number {
        let sum = 0;
        if (this.branch.newZones && this.branch.newZones.length > 0) {
            this.branch.newZones.forEach(element => {
                sum += element.tables.length;
            });
        } else {
            this.branch.zones.forEach(element => {
                sum += element.tables.length;
            });
        }
        return sum;
    }

    totalNewSum(): number {
        if (!this.branch || !this.branch.tariff) return 0;
        return (this.branch.tariff.price + this.newTablesCount() * this.branch.tariff.tablePrice) * this.branch.tariff.period +
            (this.branch.telegramIsAvailiable ? this.branch.tariff.botPrice : 0);
    }

    getStatus(): string {
        return StatusDescription[this.branch.status]
    }

    onTariffChange(tariff: Tariff) {
        this.branch.tariff = tariff;
    }

    pay() {
        this.payChange.emit();
    }

    prolong() {
        this.prolongChange.emit();
    }

    billEndDate() {
        var endDate = this.branch.bill.date;
        endDate.setMonth(endDate.getMonth() + this.branch.bill.tariff.period);
        return endDate;
    }
}