import { Component, OnInit, ViewChild } from '@angular/core';
import { SharedService } from 'src/app/Services/shared.service'
import { HttpService } from 'src/app/Services/http.service';
import { NgModel } from '@angular/forms';
import { map } from 'rxjs/operators';
import { Dialog } from 'src/app/Templates/Dialogs/Dialog';
import { Router } from '@angular/router';
import { Company } from 'src/app/Models/company.models';

@Component({
    selector: 'profile-component',
    templateUrl: './profile.component.html',
    styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
    Company: Company = null

    @ViewChild("nameI", { static: false }) nameI: NgModel;
    @ViewChild("innI", { static: false }) innI: NgModel;
    @ViewChild("ogrnI", { static: false }) ogrnI: NgModel;

    constructor(public sharedService: SharedService, private httpService: HttpService, public dialog: Dialog, private router: Router) { }

    ngOnInit() {
        this.httpService.getData("Companies", "Company", []).pipe(map(data => Company.fromJson(data))).subscribe(data => {
            this.sharedService.httpService.loading = false;
            this.Company = data;
        })
    }

    saveChanges() {
        if (this.Company && !this.submitValid) {
            this.httpService.postData("Companies", "ChangeCompany", this.Company.toJson()).pipe(map(data => Company.fromJson(data))).subscribe(data => {
                this.sharedService.httpService.loading = false;
                this.Company = data;
                if (this.sharedService.isConfirmed) {
                    this.sharedService.openSnackBar("Информация сохранена", "ОК");
                } else {
                    this.dialog.openDialog("Уведомление", "Ваша компания успешно сохранена! Теперь вы можете приступить к созданию заведения!")
                        .subscribe(data => { this.router.navigate(["/Сompanies"]); });
                }
                this.sharedService.isConfirmed = data.isConfirmed;
            });
        }
    }

    get submitValid(): boolean {
        return !this.nameI || !this.innI || !this.ogrnI ||
            this.nameI.invalid || this.innI.invalid || this.ogrnI.invalid
    }
}
