import { Component, ViewChild, TemplateRef } from '@angular/core';
import { SharedService } from '../../Services/shared.service';
import { map } from 'rxjs/operators';
import { Observable, forkJoin } from 'rxjs';
import { Router, ActivatedRoute } from '@angular/router';
import { Dialog } from 'src/app/Templates/Dialogs/Dialog';
import { NgModel } from '@angular/forms';
import { Company } from 'src/app/Models/company.models';
import { Branch } from 'src/app/Models/branch.models';

@Component({
    selector: 'index-component',
    templateUrl: './index.component.html',
    styleUrls: ['./index.component.css']
})
export class IndexComponent {
    Company: Company = null
    branches: Array<Branch>;

    companyNameFirstLetter(){
        if(this.Company && this.Company.name && this.Company.name.length > 0){
            return this.Company.name[0];
        }
        return "";
    }

    drawerOpened = true;
    drawerToggle() { this.drawerOpened = !this.drawerOpened; }

    constructor(public sharedService: SharedService, private router: Router,
        private route: ActivatedRoute, public dialog: Dialog) { 
            if (localStorage.getItem("JWTtoken")) {
                sharedService.httpService.postData("Users", "RefreshToken", { token: localStorage.getItem("JWTtoken") }).subscribe(data => {
                    if (data) localStorage.setItem("JWTtoken", data as any);
                })
            }
        }

    ngOnInit() {
        this.route.url.subscribe(x => {
            this.getData().subscribe((response) => {
                this.Company = response[0];
                this.sharedService.isConfirmed = this.Company.isConfirmed

                if(!this.sharedService.isConfirmed) {
                    this.onCompanyChange()
                }

                response[1].forEach(element => {
                    element.imagePath = element.imagePath ? "/Resources/BranchImages/" + element.imagePath : element.imagePath
                });
                this.branches = response[1];

                this.sharedService.httpService.loading = false;
            });
        });
    }

    getData(): Observable<any> {
        const response1 = this.sharedService.httpService.getData("Companies", "Company", []).pipe(map(data => Company.fromJson(data)));
        const response2 = this.sharedService.httpService.getData("Branches", "Branches", []).pipe(map(data => data as Array<any>));
        return forkJoin([response1, response2]);
    }

    moveToBranch(branch: Branch) {
        this.router.navigate(['/Service/Info', branch.id])
        this.closeDrawerOnMobile()
    }

    moveToCalendar(branch: Branch) {
        this.router.navigate(['/Service/Calendar', branch.id])
        this.closeDrawerOnMobile()
    }

    closeDrawerOnMobile() {
        if (!this.sharedService.isDesktop) {
            this.drawerOpened = false;
        }
    }

    get drawerSize() {
        if (!this.drawerOpened) {
            return this.sharedService.isDesktop ? '44px' : '0px';
        }
        return this.sharedService.isDesktop ? '300px' : '100%';
    }

    selectedBranch() {
        return this.router.url
            .replace("Service", "")
            .replace("Calendar", "")
            .replace("Info", "")
            .replace(/\//g, "");
    }

    branchSelected(branch) {
        return {
            opacity: branch.id.toString() == this.selectedBranch() ? '0.5' : '1'
        }
    }

    // Company ///////////////////////
    newCompany: Company = null;
    @ViewChild('DCompany', { static: false }) DCompany: TemplateRef<any>;
    onCompanyChange() {
        if (this.Company) {
            this.newCompany = Company.fromJson(this.Company);
            this.dialog.openDialogTemplate(this.DCompany, "350px");
        }
    }


    @ViewChild("nameI", { static: false }) nameI: NgModel;
    @ViewChild("innI", { static: false }) innI: NgModel;
    @ViewChild("ogrnI", { static: false }) ogrnI: NgModel;
    get submitValid(): boolean {
        return !this.nameI || !this.innI || !this.ogrnI ||
            this.nameI.invalid || this.innI.invalid || this.ogrnI.invalid
    }

    saveChanges() {
        if (this.Company && this.newCompany && !this.submitValid) {
            this.sharedService.httpService.postData("Companies", "ChangeCompany", this.newCompany.toJson()).pipe(map(data => Company.fromJson(data))).subscribe(data => {
                this.sharedService.httpService.loading = false;
                this.Company = data;
                this.newCompany = this.Company;
                this.sharedService.openSnackBar("Информация сохранена", "ОК");
                this.sharedService.isConfirmed = data.isConfirmed;
            });
        }
    }
}
