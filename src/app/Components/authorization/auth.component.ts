import { Component } from "@angular/core";
import { HttpService } from 'src/app/Services/http.service';

@Component({
    selector: 'authentication',
    templateUrl: './auth.component.html',
    styleUrls: ['./auth.component.css']
})
export class AuthComponent {
    constructor(public httpService: HttpService) { }
}
