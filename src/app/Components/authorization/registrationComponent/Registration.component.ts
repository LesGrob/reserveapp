import { Component, OnInit, ViewChild, TemplateRef } from "@angular/core";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { HttpService } from "../../../Services/http.service"
import { CustomValidation, IsValidEmail } from 'src/app/CustomValidators';
import { Dialog } from "../../../Templates/Dialogs/Dialog"
import { map } from "rxjs/operators";

@Component({
    selector: 'Registration',
    templateUrl: './Registration.component.html'
})
export class RegistrationComponent implements OnInit {
    constructor(private fb: FormBuilder, private httpService: HttpService, private dialogInfo: Dialog) { }

    registrationData: any = null;

    RegistrationForm: FormGroup;
    userData: any = {
        email: "", password: "", confirmPassword: ""
    };

    // Объект с ошибками, которые будут выведены в пользовательском интерфейсе
    formErrors: any = {
        "email": "",
        "password": "",
        "confirmPassword": ""
    };

    // Объект с сообщениями ошибок
    validationMessages: any = {
        "email": {
            "required": "Обязательное поле.",
            "ErrorEmail": "Неверный формат Email"
        },
        "password": { "required": "Обязательное поле." },
        "confirmPassword": {
            "required": "Обязательное поле.",
            "MatchPassword": "Пароли не совпадают."
        }
    };

    ngOnInit() {
        this.buildForm();
    }

    buildForm() {
        this.RegistrationForm = this.fb.group({
            "email": [this.userData.email, [Validators.required, IsValidEmail]],
            "password": [this.userData.password, [Validators.required]],
            "confirmPassword": [this.userData.confirmPassword, [Validators.required]]
        }, { validator: CustomValidation.IsMatchPasswords });

        this.RegistrationForm.valueChanges.subscribe(data => this.OnValueChange(data));
        this.OnValueChange();
    }


    OnValueChange(data?: any) {
        if (!this.RegistrationForm) return;
        let form = this.RegistrationForm;

        for (let field in this.formErrors) {
            this.formErrors[field] = "";
            // form.get - получение элемента управления
            let control: any = form.get(field);

            if (control && control.dirty && !control.valid) {
                let message = this.validationMessages[field];
                for (let key in control.errors) {
                    this.formErrors[field] += message[key] + " ";
                }
            }
        }
    }

    @ViewChild('DRegistration', { static: false }) DRegistration: TemplateRef<any>;
    onSubmit(form: any) {
        if (this.RegistrationForm.valid) {
            this.httpService.postData('Users', 'User', this.RegistrationForm.value)
                .pipe(map(data => data as any))
                .subscribe(
                    data => {
                        this.httpService.loading = false;
                        this.registrationData = data;
                        this.dialogInfo.openDialogTemplate(this.DRegistration, "400px").subscribe(data => {
                            location.href = './LogIn';
                        });
                    },
                    error => {
                        this.httpService.loading = false;
                        this.dialogInfo.openDialog("Ошибка", error.message);
                    });
        }
    }
}
