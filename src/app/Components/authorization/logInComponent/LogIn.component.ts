import { Component, OnInit, OnDestroy, ViewChild, TemplateRef } from "@angular/core";
import { FormGroup, Validators, FormBuilder, FormControl } from "@angular/forms";
import { AuthService } from "../../../Services/authentication.service"
import { Router } from '@angular/router';
import { Dialog } from "../../../Templates/Dialogs/Dialog";
//Validation passwords
import { IsValidEmail } from 'src/app/CustomValidators';

@Component({
    selector: 'LogIn',
    templateUrl: './LogIn.component.html'
})
export class LogInComponent implements OnInit {
    constructor(private fb: FormBuilder, private dialogForgetPass: Dialog, private authService: AuthService,
        private router: Router, public dialog: Dialog) {
        localStorage.clear();
        authService.logout();
    }

    logInForm: FormGroup;
    userData: any = { login: "", password: "" };
    ErrorMessage: string;
    Today: Date;

    // Объект с ошибками, которые будут выведены в пользовательском интерфейсе
    formErrors: any = {
        "login": "",
        "password": ""
    };

    // Объект с сообщениями ошибок
    validationMessages: any = {
        "login": { "required": "Обязательное поле." },
        "password": { "required": "Обязательное поле." }
    };

    ngOnInit() {
        this.Today = new Date(Date.now());
        this.buildForm();
    }

    buildForm() {
        this.logInForm = this.fb.group({
            "login": [this.userData.login, [Validators.required]],
            "password": [this.userData.password, [Validators.required]]
        });

        this.logInForm.valueChanges.subscribe(data => this.OnValueChange(data));
        this.OnValueChange();
    }

    OnValueChange(data?: any) {
        if (!this.logInForm) return;
        let form = this.logInForm;

        for (let field in this.formErrors) {
            this.formErrors[field] = "";
            // form.get - получение элемента управления
            let control: any = form.get(field);

            if (control && control.dirty && !control.valid) {
                let message = this.validationMessages[field];
                for (let key in control.errors) {
                    this.formErrors[field] += message[key] + " ";
                }
            }
        }
    }

    onSubmit(form: any) {
        if (this.logInForm.valid) {
            this.authService.login(this.logInForm.value);
        }
    }
    //Reset password
    @ViewChild('DForgetPassword', { static: false }) DForgetPassword: TemplateRef<any>;
    email = new FormControl('', [Validators.required, IsValidEmail, Validators.email]);
    ResetPassword() {
        this.dialogForgetPass.openDialogTemplate(this.DForgetPassword, '300px');
    }
    BeginReset() {
        if (this.email.valid)
            this.authService.httpService.postData("Users", "ResetPassword", { email: this.email.value })
                .subscribe(data => {
                    this.authService.httpService.loading = false;
                    this.dialogForgetPass.close();
                    this.dialog.openDialog("Восстановление пароля", String(data));
                });
    }
}
