import { Component, OnInit } from "@angular/core";
import { FormGroup, Validators, FormBuilder } from "@angular/forms";
import { HttpService } from "../../../Services/http.service"
import { Router } from '@angular/router';

//Validation passwords
import { CustomValidation, IsValidEmail } from 'src/app/CustomValidators';
@Component({
    selector: 'ResetPassword',
    templateUrl: './ResetPassword.component.html'
})
export class ResetPassword implements OnInit {
    constructor(private fb: FormBuilder, private http: HttpService, private router: Router) { }

    ResetPasswordForm: FormGroup;
    PasswordData: any = { email: "", code: "", password: "", confirmPassword: "" };
    ErrorMessage: string;
    UserInformation: any = { isError: false, text: "", link: "" };
    IsVisibleSaveButton: boolean = true;

    // Объект с ошибками, которые будут выведены в пользовательском интерфейсе
    formErrors: any = {
        "email": "",
        "code": "",
        "password": "",
        "confirmPassword": ""
    };

    // Объект с сообщениями ошибок
    validationMessages: any = {
        "email": {
            "required": "Обязательное поле.",
            "ErrorEmail": "Неверный формат Email"
        },
        "password": { "required": "Обязательное поле." },
        "confirmPassword": {
            "required": "Обязательное поле.",
            "MatchPassword": "Пароли не совпадают."
        }
    };

    ngOnInit() {
        this.BuildForm();
    }

    BuildForm() {
        this.ResetPasswordForm = this.fb.group({
            "email": [this.PasswordData.email, [Validators.required, IsValidEmail]],
            "code": [this.PasswordData.code, []],
            "password": [this.PasswordData.password, [Validators.required]],
            "confirmPassword": [this.PasswordData.confirmPassword, [Validators.required]]
        }, { validator: CustomValidation.IsMatchPasswords });

        this.ResetPasswordForm.valueChanges.subscribe(data => this.OnValueChange(data));
        this.OnValueChange();
    }

    OnValueChange(data?: any) {
        if (!this.ResetPasswordForm) return;
        let form = this.ResetPasswordForm;

        for (let field in this.formErrors) {
            this.formErrors[field] = "";
            // form.get - получение элемента управления
            let control: any = form.get(field);

            if (control && control.dirty && !control.valid) {
                let message = this.validationMessages[field];
                for (let key in control.errors) {
                    this.formErrors[field] += message[key] + " ";
                }
            }
        }
    }

    onSubmit(form: any) {
        if (this.ResetPasswordForm.valid) {
            //Сохранение нового пароля
            this.http.postData("Users", "ChangePassword", this.ResetPasswordForm.value).subscribe(data => {
                this.http.loading = false;
                this.UserInformation.isError = false;
                this.UserInformation.text = data + " Для входа на платформу пройдите по ";
                this.UserInformation.link = window.location.origin + "/LogIn";
                this.IsVisibleSaveButton = false;
            }, error => {
                this.http.loading = false;
                this.UserInformation.isError = true;
                this.UserInformation.text = error._body;
            });
        }
    }
}
