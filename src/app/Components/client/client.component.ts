import { Component, ElementRef, ViewChild, AfterContentInit, NgZone, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ConfirmOrderDialog } from "../../Dialogs/confirmOrder/confirmOrder.dialog"
import { HttpService } from 'src/app/Services/http.service';
import { ActivatedRoute } from '@angular/router';
import { Dialog } from 'src/app/Templates/Dialogs/Dialog';
import { Branch } from 'src/app/Models/branch.models';
import { Order } from 'src/app/Models/order.models';
import { TimeTransform } from 'src/app/Models/enums.models';

@Component({
    selector: 'client-main',
    templateUrl: './client.component.html',
    styleUrls: ['./client.component.css']
})

export class ClientComponent implements AfterContentInit, OnInit {
    @ViewChild('planImage', { static: false }) planImage: ElementRef;
    @ViewChild('datePicker', { static: false }) datePicker: any;

    get hasData(): boolean { return this.branch && this.branch.zones.length > 0; }

    private branchId: number = 0;
    branch: Branch = new Branch();
    selectedDate: Date = new Date();
    selectedTime: String = "12:00";
    peopleCount = 1;
    zoneIndex: number = 0;

    koeff = 1;

    get peopleMax(): number {
        if (!this.branch) return 1;

        var sits = this.branch.zones.map(x => x.tables.map(y => y.sits));
        var count = 0;
        for (let zone of this.branch.zones) {
            for (let table of zone.tables) {
                if (table.selected) count += table.sits;
            }
        }
        return count;
    }

    s_point(pointX: number, pointY: number): { x: number, y: number } { return { x: pointX * this.koeff, y: pointY * this.koeff }; }
    s_length(length: number): number { return length * this.koeff; }

    constructor(public dialog: MatDialog, public acceptDialog: Dialog,
        private route: ActivatedRoute, public httpService: HttpService) { }

    ngOnInit() {
        this.route.params.subscribe(data => {
            this.branchId = data.id;
            this.getData();
        })
    }

    ngAfterContentInit() {
        window.onresize = (e) => {
            this.imageResize();
        }
    }

    selectTable(index: number) {
        if (index >= 0 && index < this.branch.zones[this.zoneIndex].tables.length) {
            let table = this.branch.zones[this.zoneIndex].tables[index];
            if (table.availiable) {
                table.selected = !table.selected;
            }
        }
    }

    zoneChange(event) {
        if (event.index >= 0 && event.index < this.branch.zones.length) {
            this.zoneIndex = event.index;
        }
    }

    changeDate($event) {
        this.selectedDate = $event.value;
        this.getData();
    }
    changeTime($event) {
        this.selectedTime = $event;
        this.getData();
    }

    confirm() {
        const dialogRef = this.dialog.open(ConfirmOrderDialog, {
            width: '300px',
            data: { count: this.getChosenCount(), sum: this.getSumCount() }
        });

        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                let order = Order.fromJson(result);
                order.branch.id = this.branch.id;
                order.time = this.selectedDate;
                order.timeMinutes = TimeTransform.toMinutes(this.selectedTime);
                order.tables = [];
                if (this.branch && this.branch.zones.length > 0) {
                    this.branch.zones.forEach(zone => {
                        zone.tables.forEach(table => {
                            if (table.selected) order.tables.push(table.id);
                        });
                    });
                }
                let data = order.toJson();
                this.httpService.postData("Client", "Order", data).subscribe(data => {
                    this.httpService.loading = false;
                    this.acceptDialog.openDialog("Подтверждение", "Ваша бронь подтверждена!");
                });
            }
        });
    }

    //DATA
    getData() {
        let regex = "([0-9]?[0-9])\:([0-9]?[0-9])";
        let hours = parseInt(this.selectedTime.match(regex)[1]);
        let minutes = parseInt(this.selectedTime.match(regex)[2]);
        let time = hours * 60 + minutes;
        let date = this.selectedDate.toDateString();
        this.httpService.getData("Client", "Branch", [this.branchId, date, time]).subscribe(data => {
            this.httpService.loading = false;
            this.branch = Branch.fromJson(data);
        })
    }
    getChosenCount() {
        let i = 0;
        if (this.branch && this.branch.zones.length > 0) {
            this.branch.zones.forEach(zone => {
                zone.tables.forEach(table => {
                    if (table.selected) i++;
                });
            });
        }
        return i;
    }

    getSumCount() {
        let i = 0;
        if (this.branch && this.branch.zones.length > 0) {
            this.branch.zones.forEach(zone => {
                zone.tables.forEach(table => {
                    if (table.selected) i += table.depositCost;
                });
            });
        }
        return i;
    }


    //Image handlers
    imageResize() {
        this.koeff = 1;
        if (this.planImage) {
            let wK = this.planImage.nativeElement.clientWidth / this.branch.zones[this.zoneIndex].imageWidth;
            let hK = this.planImage.nativeElement.clientHeight / this.branch.zones[this.zoneIndex].imageHeight;
            this.koeff = Math.min(hK, wK);
        }
    }

    //Styles
    getTableStyle(index: number) {
        if (this.branch.zones[this.zoneIndex] != null && index >= 0 && index < this.branch.zones[this.zoneIndex].tables.length) {
            let item = this.branch.zones[this.zoneIndex].tables[index];
            let point = this.s_point(item.positionX, item.positionY);

            let color;
            let bColor;

            if (!item.availiable) {
                color = this.branch.zones[this.zoneIndex].tableDisabledColor;
                bColor = this.branch.zones[this.zoneIndex].tableDisabledBColor;
            } else if (item.selected) {
                color = this.branch.zones[this.zoneIndex].tableActiveColor;
                bColor = this.branch.zones[this.zoneIndex].tableActiveBColor;
            } else {
                color = this.branch.zones[this.zoneIndex].tableColor;
                bColor = this.branch.zones[this.zoneIndex].tableBColor;
            }

            return {
                'transform': 'translate3d( ' + point.x + 'px, ' + point.y + 'px, 0px)',
                'width': this.s_length(item.width) + 'px',
                'height': this.s_length(item.height) + 'px',
                'border-radius': item.radius + 'px',
                'background-color': color,
                'border-color': bColor,
                'color': bColor
            };
        }
        return {};
    }
}