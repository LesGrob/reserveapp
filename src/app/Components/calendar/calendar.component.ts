import { Component, TemplateRef, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { SharedService } from 'src/app/Services/shared.service';
import { CalendarEvent } from 'calendar-utils';
import { CalendarView } from 'angular-calendar';
import { map } from 'rxjs/operators';
import { Dialog } from 'src/app/Templates/Dialogs/Dialog';
import { OrderFull } from 'src/app/Models/order.models';

@Component({
    selector: 'calendar-component',
    templateUrl: './calendar.component.html',
    styleUrls: ['./calendar.component.css']
})

export class CalendarComponent {
    private branchId: number = 0;

    events: CalendarEvent[] = [];
    orders: OrderFull[] = [];

    selectedOrder: OrderFull = null;

    view: CalendarView = CalendarView.Month;
    viewDate = new Date();

    locale: string = 'ru';

    private routeSubscribtion: Subscription;

    constructor(private router: Router, private route: ActivatedRoute,
        public sharedService: SharedService, public dialog: Dialog) { }

    ngOnDestroy() { this.routeSubscribtion.unsubscribe(); }
    ngOnInit() {
        this.sharedService.httpService.loading = true;
        this.routeSubscribtion = this.route.params.subscribe(data => {
            this.branchId = data.id;
            this.sharedService.httpService.getData("Calendar", "Orders", [this.branchId, this.viewDate.toDateString()]).pipe(map(data => data as Array<any>)).subscribe(data => {
                this.orders = data;
                this.events = []
                data.forEach(element => {
                    let order = OrderFull.fromJson(element);
                    this.events.push(order.toCalendarEvent());
                });
                this.sharedService.httpService.loading = false;
            })
        })
    }

    calendarStyle() {
        return {
            'height': 'calc(100% - ' + (this.sharedService.isDesktop ? '52' : '116') + 'px)'
        }
    }


    dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
        this.viewDate = date;
    }

    changeMonth() {
        this.sharedService.httpService.loading = true;
        this.sharedService.httpService.getData("Calendar", "Orders", [this.branchId, this.viewDate.toUTCString()]).pipe(map(data => data as Array<any>)).subscribe(data => {
            this.orders = data;
            this.events = []
            data.forEach(element => {
                let order = OrderFull.fromJson(element);
                this.events.push(order.toCalendarEvent());
            });
            this.sharedService.httpService.loading = false;
        })
    }

    @ViewChild('DEventInfo', { static: false }) DEventInfo: TemplateRef<any>;
    eventClicked(event) {
        if (event && event.id) {
            this.selectedOrder = this.orders.filter(x => x.id == event.id)[0];
            if (this.selectedOrder) this.dialog.openDialogTemplate(this.DEventInfo, "300px");
        }
    }
}