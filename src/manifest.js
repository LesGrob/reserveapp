var pathname = window.location.pathname;

if (!(pathname.includes("app"))) {
    document.querySelector('#manifest').setAttribute('href', "manifest.json");
} else {
    var myDynamicManifest = {
        "name": "Dengrad",
        "short_name": "Dengrad",
        "theme_color": "#913349",
        "background_color": "#f5f5f5",
        "display": "standalone",
        "scope": "/",
        "start_url": pathname,
        "icons": [
            {
                "src": "assets/icons/icon-72x72.png",
                "sizes": "72x72",
                "type": "image/png"
            },
            {
                "src": "assets/icons/icon-96x96.png",
                "sizes": "96x96",
                "type": "image/png"
            },
            {
                "src": "assets/icons/icon-128x128.png",
                "sizes": "128x128",
                "type": "image/png"
            },
            {
                "src": "assets/icons/icon-144x144.png",
                "sizes": "144x144",
                "type": "image/png"
            },
            {
                "src": "assets/icons/icon-152x152.png",
                "sizes": "152x152",
                "type": "image/png"
            },
            {
                "src": "assets/icons/icon-192x192.png",
                "sizes": "192x192",
                "type": "image/png"
            },
            {
                "src": "assets/icons/icon-512x512.png",
                "sizes": "512x512",
                "type": "image/png"
            }
        ]
    };
    const stringManifest = JSON.stringify(myDynamicManifest);
    const blob = new Blob([stringManifest], { type: 'application/json' });
    const manifestURL = URL.createObjectURL(blob);

    document.querySelector('#manifest').setAttribute('href', manifestURL);
}